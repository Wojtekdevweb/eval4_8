# Évaluation compétences 4 et 8
Création du site vitrine de la promo Simplon Cahors avec un CMS au choix. 

## Objectifs

- Savoir installer et utiliser un CMS.
- Savoir développer un thème pour un CMS.
- Savoir développer une extension pour un CMS.

## Réalisation

- Durée : **Mercredi 28 août 2019 9h00 - jeudi 29 août 2019 17h00**. Tout retard ne sera pas corrigé.
- Groupe : **En solo**

## Contraintes

- CMS au choix : Wordpress ou Drupal

## Rendu

- **Le code source du site.** (Attention à ne versionner que le contenu nécessaire.)

Plus au choix :

- **Un dump de la base de données.**
- **Un fichier readme.md** expliquant précisément comment faire fonctionner le projet en local (installation des dépendances, connexion à la BDD, etc.)

OU : 

- **L'adresse url du site sur le serveur preprod fourni par Simplon.**

## Réalisation

### Partie 1 : arborescence du site
  
Le site devra comporter les éléments suivants. Vous pouvez utiliser du faux texte et des fausses images.
- Un header avec le titre "Simplon Cahors", un logo et une image de fond.
- Un footer avec un lien vers une page "Mentions légales".
- Une barre de navigation avec les liens vers les pages "Accueil", "Apprenants" et "Projets".
- Une page "Accueil" avec une image et un texte de bienvenue.
- Une page "Apprenants" avec les  photos de tous les apprenants et un paragraphe de présentation pour chacun.
- Une page "Projets" présentant au moins 5 projets avec pour chaque projet un paragraphe, une image, une date, et un lien "Voir le projet". 
- Une page détail pour chaque projet, accessible depuis le lien "Voir le projet".

### Partie 2 : gestion des rôles

- Créer 2 comptes utilisateur : "admin" qui a les droits de modification/suppression sur la totalité du site et "rédacteur" qui peut ajouter ou retirer du contenu sans droit de modifier la structure du site.

### Partie 3 : utilisation d'un module préexistant

- Installer et paramétrer un module permettant d'afficher la galerie des apprenants sous forme de carousel.

### Partie 4 : création de thème

- Créer un thème custom intégrant du CSS personnalisé pour le header et le footer.

### Partie 5 : création d'un module custom

- Créer un module permettant d'afficher une page contenant le texte "Hello world !" quand l'utilisateur tape l'url [racine-du-site]/helloworld. Lorsque le module est désactivé, l'url doit renvoyer une erreur 404.



## Critères de validation

### Compétence 4 : Réaliser une interface utilisateur avec une solution de gestion de contenu ou e-commerce

- Le site est installé et paramétré conformément au besoin client.
- Les comptes utilisateurs sont créés avec leurs droits et rôles dans le respect des règles de sécurité.
- La structure du site est conforme au besoin client.
- L’aspect visuel respecte la charte graphique du client et est adapté à l’équipement de l’utilisateur.
- Le site est publié sur un serveur.
- Le site respecte les règles de référencement naturel.
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise.
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...).
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité.
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs.

### Compétence 8 : Élaborer et mettre en oeuvre des composants dans une application de gestion de contenu ou e-commerce
- Les bonnes pratiques de développement objet sont respectées.
- Les composants complémentaires ou réalisés s’intègrent dans l’environnement de l’application.
- Les composants serveur contribuent à la sécurité de l’application.
- Le code source est documenté ou auto-documenté.
- Les tests garantissent que les pages web répondent aux exigences décrites dans le cahier des charges [ou dans le dossier technique].
- L'application web est publiée sur un serveur.
- L’objet de la recherche est exprimé de manière précise en langue française ou anglaise.
- La documentation technique liée aux technologies associées, en français ou en anglais, est comprise (sans contre-sens, ...).
- La démarche de recherche permet de résoudre un problème technique ou de mettre en oeuvre une nouvelle fonctionnalité.
- La veille sur les vulnérabilités connues permet d’identifier et corriger des failles potentielles.
- Le partage du résultat de recherche et de veille est effectué, oralement ou par écrit, avec ses pairs.