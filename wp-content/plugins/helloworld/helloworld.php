<?php
/*
Plugin Name: Hello World
Plugin URI:
Description: My First Plugin Word Press
Author: Wojtek
Version: 1
Author URI: 
*/

// Write a new permalink entry on code activation
register_activation_hook( __FILE__, 'plugincustom_activation' );
function plugincustom_activation() {
        plugincustom_plugin_custom();
        flush_rewrite_rules(); // Update the permalink entries in the database, so the permalink structure needn't be redone every page load
}

// If the plugin is deactivated, clean the permalink structure
register_deactivation_hook( __FILE__, 'plugincustom_deactivation' );
function plugincustom_deactivation() {
        flush_rewrite_rules();
}


// And now, the code that do the magic!!!
// This code create a new permalink entry
add_action( 'init', 'plugincustom_plugin_custom' );
function plugincustom_plugin_custom() {
        add_rewrite_tag( '%helloworld%', '([^/]+)' );
        add_permastruct( 'helloworld', '/%helloworld%' );
}

// The following controls the page content
add_action( 'template_redirect', 'plugincustom_display' );
function plugincustom_display() {
        if ($query_var = get_query_var('helloworld')) {
                header("Content-Type: text/plain");
                echo 'Hello World !';
                exit; // Don't forget the exit. If so, WordPress will continue executing the template rendering and will not fing anything, throwing the 'not found page' 
        }
}


